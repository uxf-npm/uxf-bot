# @uxf/bot

Install package

```
yarn global add @uxf/bot
```

Set environment variables

- `GITLAB_URL`
- `GITLAB_PROJECT_ID`
- `GITLAB_TOKEN`

and then run

```
$ uxf-bot-server
```
