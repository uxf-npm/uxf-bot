#!/usr/bin/env node
require("../src/cli")()
    .then(exitCode => {
        process.exitCode = exitCode;
    })
    .catch(() => {
        process.exitCode = 1;
    });
