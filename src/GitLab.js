const { env } = require("process");
const { create } = require("axios");

const axios = create({
    baseURL: `${env.GITLAB_URL}/api/v4`,

    headers: {
        Authorization: `Bearer ${env.GITLAB_TOKEN}`,
    },
});

async function loadCommits() {
    const { data } = await axios.get(`/projects/${env.GITLAB_PROJECT_ID}/repository/compare`, {
        params: {
            from: "master",
            to: "develop",
        },
    });
    return data.commits;
}

async function loadIssues(ids) {
    const { data } = await axios.get(`/projects/${env.GITLAB_PROJECT_ID}/issues`, {
        params: {
            iids: ids,
        },
    });
    return data;
}

module.exports = {
    loadCommits,
    loadIssues,
};
