const { env } = require("process");
const GitLab = require("./GitLab");

function getCommitMessage(commits) {
    return commits
        .map(({ title, short_id, web_url, author_email }) => `• ${title} <${web_url}|${short_id}> (${author_email})`)
        .join("\n");
}

async function run() {
    const commits = (await GitLab.loadCommits()).map(c => {
        const parsed = c.title.match(/#(\d+)/) || [];
        const [_, value] = parsed;
        return { ...c, issueId: value ? Number.parseInt(value) : undefined };
    });

    const issueIds = commits.map(c => c.issueId).filter(i => !!i);

    const issues = (await GitLab.loadIssues(issueIds)).map(i => ({ ...i, commits: [] }));
    const commitsWithoutIssues = [];

    for (const commit of commits) {
        const { issueId, title } = commit;

        if (issueId) {
            const issue = issues.find(i => i.iid === issueId);
            if (issue) {
                issue.commits.push(commit);
            } else {
                commitsWithoutIssues.push(commit);
            }
        } else {
            commitsWithoutIssues.push(commit);
        }
    }

    const blocks = [
        {
            type: "section",
            text: { type: "mrkdwn", text: "Na nasazení je připraveno" },
        },
    ];

    for (const issue of issues) {
        const { title: issueTitle, state: issueState, commits: issueCommits } = issue;
        blocks.push({
            type: "section",
            text: {
                type: "mrkdwn",
                text: `*${issueTitle}*\n${getCommitMessage(issueCommits)}`.substr(0, 3000),
            },
            // accessory: {
            //     type: "button",
            //     text: {
            //         text: "Detail",
            //     },
            // },
        });
    }

    blocks.push({
        type: "section",
        text: {
            type: "mrkdwn",
            text: `*Commity bez issue*\n${getCommitMessage(commitsWithoutIssues.slice(0, 18))}`.substr(0, 3000),
        },
    });

    return {
        blocks: blocks,
    };
}

module.exports = {
    run,
};
