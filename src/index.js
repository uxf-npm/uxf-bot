const express = require("express");
const NextRelease = require("./NextRelease");

module.exports = function () {
    const app = express();
    const port = 3000;

    app.post("/slack/next-release", (_, res) => {
        NextRelease.run()
            .then(data => res.send(data))
            .catch(console.error);
    });

    app.listen(port, () => {
        console.log(`UXF Bot Server listening on port ${port}`);
    });
};
