const { argv, env } = require("process");

module.exports = async () => {
    const cli = require("yargs")
        .command("$0", "UXF Bot Server", yargs => {
            yargs.demandCommand(0, 0).usage(`UXF Bot Server
Usage:
  uxf-bot-server [options]
  
Environment variables:
  GITLAB_URL        - required
  GITLAB_TOKEN      - required
  GITLAB_PROJECT_ID - required`);
        })
        .option("h", { alias: "help", group: "Options" })
        .strict(false)
        .exitProcess(false);

    try {
        const { help } = cli.parse(argv.slice(2));

        if (Boolean(help)) {
            return 0;
        }

        if (!env.GITLAB_URL) {
            console.log("GitLab url must be set. Use environment variable GITLAB_URL.");
            return 1;
        }

        if (!env.GITLAB_PROJECT_ID) {
            console.log("Project id must be set. Use environment variable GITLAB_PROJECT_ID.");
            return 1;
        }

        if (!env.GITLAB_TOKEN) {
            console.log("Environment variable GITLAB_TOKEN is empty.");
            return 1;
        }

        await require("./index")();
    } catch (e) {
        console.error(e);
        return 1;
    }
};
